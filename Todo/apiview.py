from django.shortcuts import redirect
from rest_framework import generics

from rest_framework.permissions import IsAuthenticated, AllowAny
from Todo.serializers import TaskSerializer, TaskSerializerExecute, RegisterSerializer
from Todo.models import Task
from Users.models import MyUser
from .tasks import send


class TaskApiView(generics.ListCreateAPIView):
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        qs = Task.objects.filter(is_complete=False).filter(author=self.request.user)
        return qs

class TaskDetaiUpdDelApiView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

class TaskExecute(generics.RetrieveUpdateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Task.objects.all()
    serializer_class = TaskSerializerExecute

    def put(self, request, *args, **kwargs):
        task_name = request.data.get('title')
        status = request.data.get('is_complete')
        if status == 'true':
            status = 'Выполнено'
        else: status == 'Не выполнено'
        current_user = request.user
        send.delay(current_user.email, task_name, status)
        return self.update(request, *args, **kwargs)

class RegisterUserAPIView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)
        return redirect('todo_api')
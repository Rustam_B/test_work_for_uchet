from test_work_for_uchet.celery import app
from django.core.mail import send_mail
from Todo.models import Task
from Todo.serializers import TaskSerializerExecute

@app.task()
def send(user_mail, task_name, status):
    send_mail(
        subject=f'Ваш трекер',
        message= f"Статус задачи '{task_name}' изменен на {status}",
        from_email='dizzy00013@yandex.ru',
        recipient_list=(user_mail,),
        fail_silently=False
    )

